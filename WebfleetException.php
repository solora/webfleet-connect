<?php

namespace solora\webfleet;

/**
 * A Webfleet exception.
 */
class WebfleetException extends \Exception
{
    // we have alphanumeric error codes too in WEBFLEET.connect
    private $errorCode = null;
    
    /**
     * constructor.
     * @param string $message exception message.
     * @param string $code Webfleet error code.
     * @param \Exception $previous parent exception.
     */
    public function __construct($message, $code, \Exception $previous = null)
    {
        parent::__construct($message, null, $previous);
        $this->errorCode = $code;
    }

    /**
     * Returns the webfleet error code.
     * @return string webfleet error code.
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
