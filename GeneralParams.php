<?php

namespace solora\webfleet;

/**
 * holds the the general parameters.
 */
class GeneralParams
{
    public $lang         = null;
    public $separator    = null;
    public $useISO8601   = null;
    public $columnfilter = null;
    public $useUTF8      = null;

    /**
     * constructor
     * @param string $lang the language to use.
     */
    public function __construct($lang="en", $useUTF8="true")
    {
        $this->lang  = $lang;
        $this->useUTF8 = $useUTF8;
    }
}
