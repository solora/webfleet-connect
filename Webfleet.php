<?php
namespace solora\webfleet;

use solora\webfleet\connector\CsvConnector;

/**
 * base class of the WEBFLEET.connect API
 */
class Webfleet
{
    /**
     * Constructs a new connection to WEBFLEET.connect.
     * @param string $proto the protocol to use.
     * Currently only "csv" is supported.
     * @param string $url URL to the server.
     * @param AuthParams $auth authentication parameters.
     * @param GeneralParams $general general parameters.
     * @return Connector connector object.
     */
    public static function connect($proto="csv", $url, AuthParams $auth, GeneralParams $general = null)
    {
        switch ($proto){
            case 'csv':
            default:
                return new CsvConnector($url, $auth, $general);
        }
    }
}
