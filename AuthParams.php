<?php

namespace solora\webfleet;

/**
 * holds the the authentication parameters.
 */
class AuthParams
{
    public $account  = null;
    public $username = null;
    public $password = null;
    public $apikey   = null;
    
    /**
     * constructor
     * @param string $account the accountname.
     * @param string $username the username.
     * @param string $password the password.
     */
    public function __construct($account, $username, $password)
    {
        $this->account  = $account;
        $this->username = $username;
        $this->password = $password;
    }
}
