<?php

namespace solora\webfleet\connector;

use solora\webfleet\Action;

/**
 * common base interface of the connector.
 */
interface Connector
{
    /**
     * Executes the given action and returns its result.
     * In case of an error, a WebfleetException is thrown.
     * @param Action $action the action to execute.
     * @return array an array of generic objects containing the result
     * (use "print_r" to print their properties) or TRUE
     * if the action executed successfully without response.
     */
    public function execute(Action $action);
}

?>