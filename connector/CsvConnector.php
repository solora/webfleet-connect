<?php

namespace solora\webfleet\connector;

use solora\webfleet\Action;
use solora\webfleet\AuthParams;
use solora\webfleet\GeneralParams;
use solora\webfleet\WebfleetException;

/**
 * implementation of the webfleet connector - using the CSV protocol.
 */
class CsvConnector implements Connector
{
    private static $HTTP_CODE   = "Http-Code";
    private static $SEP_DEFAULT = ";";
    private static $EMPTY = array("63",          // CSV
                                  "9198",        // SOAP
                                  "WFCQCS_E0003" // message queues
                                 );

    private $url     = null;
    private $auth    = null;
    private $general = null;

    public $proxyHostname = null;
    public $proxyPort     = null;
    public $proxyUsername = null;
    public $proxyPassword = null;

    /**
     * constructor.
     * @param $url the servers URL.
     * @param AuthParams $auth authentication parameters.
     * @param GeneralParams $general general parameters.
     */
    public function __construct($url, AuthParams $auth, GeneralParams $general=null)
    {
        $this->url     = $url;
        $this->auth    = $auth;
        $this->general = $general;
    }

    /**
     * (non-PHPdoc)
     * @see webfleet.Connector::execute()
     */
    public function execute(Action $action)
    {
        ///////////////////////////////////////////////////////////////////////////////
        // fire request
        $allParams = array_merge((array)$this->auth,                    // authentication params
                                 (array)$this->general,                 // general parameters
                                 array("action" => $action->getName()), // action name
                                 $action->getParams());                 // action params


        $url = $this->url."?".http_build_query($allParams);

        $ctx = null;
        if ($this->proxyHostname)
        {
            // we have proxy settings. Apply to context.
            $http = array();
            $http["proxy"] = "tcp://".$this->proxyHostname.":".($this->proxyPort ? $this->proxyPort : 8080);
			//For SSL full uri true does not work -  so disabled it
            $http["request_fulluri"] = false;

            // proxy auth required?
            if ($this->proxyUsername)
            {
                $http["header"] = "Proxy-Authorization: Basic ".base64_encode($this->proxyUsername.":".$this->proxyPassword);
            }
            $data = array("http" => $http);
            $ctx = stream_context_create($data);
        }

        $raw = file_get_contents($url,false,$ctx);
        //
        ///////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////
        // check response status

        // check HTTP response code
        $http = $this->parseHttpHeader($http_response_header); // "$http_response_header" is provided by PHP itself (through file_get_contents)
        $code = $http[CsvConnector::$HTTP_CODE];
        if ($code != '200')
            throw new \Exception("got HTTP error ".$code." for request: ".$url,$code);

        // void action?
        if (!($raw) || strlen($raw) === 0)
            return true;

        // split raw data into lines, normalize different types of line wraps and remove last wrap
        $raw = rtrim(str_replace("\r\n","\n",$raw));

        // count lines. 1 line indicates an error
        $lines = explode("\n",$raw);
        if (sizeof($lines) === 1)
        {
            // format of error messages:
            // code, message
            $comma = strpos($lines[0],",");
            if (!$comma)
                throw new \Exception("got unknown response \"".$lines[0]."\" for request: ".$url);

            $code = substr($lines[0],0,$comma);
            $msg  = substr($lines[0],$comma+1);

            // known code for "empty result"?
            if (array_search($code,CsvConnector::$EMPTY))
                return array(); // return empty list

            // otherwise throw error.
            throw new WebfleetException($msg,$code);
        }
        //
        ///////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////
        // build result document
        $result = array();

        // separator char
        $sep = $this->getSeparator();

        // first line contains the column names
        $columns = $this->split(array_shift($lines),$sep);

        // payload
        foreach($lines as $line)
        {
            $values = $this->split($line,$sep);
            $item = array();
            for ($i=0;$i<sizeof($columns);++$i)
            {
                if (!$columns[$i]) // should not happen
                    continue;
                $item[$columns[$i]] = $values[$i];
            }

            // add to result
            array_push($result,(object)$item);
        }
        //
        ///////////////////////////////////////////////////////////////////////////////

        return $result;
    }

    /**
     * parses the http response header and returns them as an associative array.
     * @param array $headers the header lines.
     * @return array an associative array containing the parsed headers.
     */
    private function parseHttpHeader($headers)
    {
        $parsed = array();

        // 1) parse http code
        // this is always the first line of the header.
        $first = array_shift($headers);
        if (!preg_match("/HTTP\/1\.[0|1] ([0-9]{3}) .*/",$first,$matches))
        {
            trigger_error("unable to parse HTTP response code from: ".$first,E_USER_WARNING);
        }
        else
        {
            $parsed[CsvConnector::$HTTP_CODE] = $matches[1];
        }


        // 2) parse remaining headers
        foreach($headers as $line)
        {
            $colon = strpos($line,":");
            if ($colon)
            {
                $parsed[substr($line,0,$colon)] = trim(substr($line,$colon+1));
            }
        }

        return $parsed;
    }

    /**
     * Splits the line into its columns.
     * @param string $line the raw csv line.
     * @param string $sep the separator char. usually ";".
     * @return array containing the column values.
     */
    private function split($line,$sep)
    {
        // voodoo regex to split the CSV columns
        $regex = "/".$sep."(?=([^\"]*\"[^\"]*\")*(?![^\"]*\"))/";
        $values = preg_split($regex,$line);

        // pos-processing
        for ($i=0;$i<sizeof($values);++$i)
        {
            // remove quotings
            $values[$i] = substr($values[$i],1,-1);

            // unescape double-quotes
            $values[$i] = str_replace("\"\"","\"",$values[$i]);
        }
        return $values;
    }

    /**
     * returns the separator char for columns.
     */
    private function getSeparator()
    {
        $sep = $this->general ? $this->general->separator : null;

        if (!$sep)
            return CsvConnector::$SEP_DEFAULT;

        switch ($sep)
        {
            case "1":
                return "\t";
            case "2":
                return " ";
            case "3":
                return ",";
        }
        return CsvConnector::$SEP_DEFAULT;
    }
}

?>