<?php

namespace solora\webfleet;

/**
 * Implementation of a generic action.
 *
 */
class Action
{
    private $name   = null;
    private $params = null;
    
    /**
     * constructor.
     * @param string $name name of the action.
     * @param array $params associative array containing the actions parameters.
     */
    public function __construct($name, $params = array())
    {
        $this->name   = $name;
        $this->params = $params;
    }
    
    /**
     * Returns the WEBFLEET.connect name of the action.
     * @return string the name of the action.
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Returns an associative array containing the action parameters.
     * @return array the action parameters.
     */
    public function getParams()
    {
        return $this->params;
    }
}
